package com.company.products.packs;

import com.vub.lab4.products.Product;
import lombok.Getter;

@Getter
public class PillsPack extends Pack {

    public PillsPack(String name, String serialNumber, int packSize) {
        super(name, serialNumber, packSize);
    }

    @Override
    public String getDescription() {
        return getFullProductName();
    }
}
