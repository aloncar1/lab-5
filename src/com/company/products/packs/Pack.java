package com.company.products.packs;

import com.vub.lab4.products.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public abstract class Pack extends Product {

    protected int packSize;

    public Pack(String name, String serialNumber, int packSize) {
        super(name, serialNumber);
        this.packSize = packSize;
    }
}
