package com.company;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public class Company {

    private String companyName;
    private Group group;
    private List<Branch> branches;

    public Company(String companyName, Group group) {
        this.companyName = companyName;
        this.group = group;
        this.branches = new ArrayList<>();
    }

    public void addBranch(Branch branch) {
        branches.add(branch);
    }

    public void doStockTaking(String groupPrefix) {
        for (Branch branch: branches) {
            var companyPrefix = new StringBuilder()
                    .append(companyName)
                    .append(Constants.STOCKTAKING_DISPLAY_SEPARATOR)
                    .toString();

            System.out.print(groupPrefix);
            System.out.print(companyPrefix);
            branch.doStockTaking();
        }
    }

  public void closeBranch(Branch branch){

        branches.remove(branch);
  }


}
